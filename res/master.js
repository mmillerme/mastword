(function ($) {
	if (!localStorage['MastWord.phrases'] || !localStorage['MastWord.phrases'].length) {
		localStorage['MastWord.phrases'] = JSON.stringify({});
	}

	var load_phrases = function () {
		var $phrases = $('#phrases').empty();
		$.each(JSON.parse(localStorage['MastWord.phrases']), function (phrase, length) {
			$('<option>').attr('value', phrase).appendTo($phrases);
		});
	};
	load_phrases();

	var sanitize_phrase = function (val) {
		return $.trim(val.toLowerCase())
	};

	$('#phrase')
		.change(function () {
			var val = sanitize_phrase($(this).val());
			if (!val.length) {
				return;
			}
			var phrases = JSON.parse(localStorage['MastWord.phrases']);
			if (!phrases[val]) {
				phrases[val] = sanitize_length($('#length').val());
				localStorage['MastWord.phrases'] = JSON.stringify(phrases);
				load_phrases();
			}
		})
		.on('change input', function () {
			var val = sanitize_phrase($(this).val());
			if (!val.length) {
				return;
			}
			var phrases = JSON.parse(localStorage['MastWord.phrases']);
			if (phrases[val]) {
				$('#length').val(phrases[val]).keyup();
			}
		});

	var sanitize_length = function (val) {
		val = parseInt(val);
		return isNaN(val) || val < 6 || val > 30 || val % 1 !== 0 ? 16 : val;
	};

	$('#length').blur(function () {
		$(this).val(sanitize_length($(this).val()));
	});

	$('#length').on('keyup input', function () {
		var phrases = JSON.parse(localStorage['MastWord.phrases']);
		phrases[sanitize_phrase($('#phrase').val())] = sanitize_length($(this).val());
		localStorage['MastWord.phrases'] = JSON.stringify(phrases);
	});

	$('#password').keyup(function () {
		$('#password-view').text($(this).val());
	});

	$('#show-password')
		.mousedown(function () {
			$('#password-view').show();
		})
		.mouseup(function () {
			$('#password-view').hide();
		});

	$('#phrase, #password, #length').on('keyup input', function () {
		var $result = $('#result').empty();

		var password = $('#password').val();
		var phrase = $('#phrase').val();
		if (!password.length || !phrase.length) {
			return;
		}

		var half = Math.floor(password.length / 2);
		var length = sanitize_length($('#length').val());
		var hash = Whirlpool(length + password.substr(0, half) + phrase + password.substr(half)).toLowerCase();

		var chunks = $.map(hash.match(/.{1,8}/g), function (chunk) {
			var charset = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
			var number = parseInt(chunk, 16);
			var result = '';

			var base = charset.length;
			while (number >= 1) {
				var n = Math.floor(number / base);
				result = charset[number - base * n] + result;
				number = n;
			}

			result = result.length < 6 ? new Array(6 - result.length + 1).join('0') + result : result;
			return result;
		});

		var result = chunks.join('');
		var length = sanitize_length($('#length').val());
		if (result.length < length) {
			result += 'PAfJBiXfKR4JG2hc'.substr(0, length - result.length);
		} else if (result.length > length) {
			result = result.substr(0, length);
		}

		$result.text(result);
	});
})(jQuery);